package space.gretawang.tp.books;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class MonkAndPhilosophy {
    public static void main(String args[]) {
        File file = new File(MonkAndPhilosophy.class.getResource("/").getPath() + "books\\TheMonk.txt");
        List<String> lines;
        try {
            lines = FileUtils.readLines(file, "UTF-8");
            List<String> roots = new ArrayList<>(Arrays.asList("urg", "erg", "org"));
            Map<String, List<String>> wordMap = new HashMap<>();


            int wordCount = 0;
            for (String line : lines) {
                for (String root : roots) {
                    if (line.trim().contains(root)) {
                        if (!wordMap.containsKey(root)) {
                            wordMap.put(root, new ArrayList<>());
                        }
                        wordMap.get(root).add(line);
                        wordCount++;
                    }
                }
            }

            wordMap.forEach((root, wordList) -> {
                wordList.forEach((eachWord ->
                        System.out.printf("%s - %s\n", root, eachWord)));
            });
            System.out.println(wordCount);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
