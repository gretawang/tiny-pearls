package space.gretawang.tp.roothandler;

import org.apache.commons.io.FileUtils;
import space.gretawang.tp.moviewords.WordFrequencyHandler;
import space.gretawang.tp.moviewords.WordStructure;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

public class RootDemo {
    public static void main(String args[]) {
        File file = new File(WordFrequencyHandler.class.getResource("/").getPath() + "\\root.txt");

        List<String> lines;
        try {
            lines = FileUtils.readLines(file, "UTF-8");

            int wordCount = 1;
            for (String line : lines) {
//                String[] words = line.split(" ");
//                for (String word : words) {
////                    if (word.startsWith("-") && word.endsWith("-")) {
//                        System.out.println(String.format("%d : %s", wordCount, word));
//                        wordCount++;
////                    }
//                }
                StringTokenizer token=new StringTokenizer(line, " \t,，");
                while(token.hasMoreTokens()){
                    String word=token.nextToken();
                    String lowerWord = word.toLowerCase().trim();
                    if (lowerWord.startsWith("-") && lowerWord.endsWith("-")) {
                        System.out.println(String.format("%d : %s", wordCount, lowerWord));
                        wordCount++;

                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
