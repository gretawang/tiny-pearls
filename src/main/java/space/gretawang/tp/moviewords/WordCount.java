package space.gretawang.tp.moviewords;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;



public class WordCount {
    public static void main(String arg[]){
        try {
            Map<String, Long> wordFrequencyMap = WordFrequencyHandler.getWordFrequencyMap();


            File file = new File("C:\\Users\\leandev\\Desktop\\Test.txt");

            List<String> lines = FileUtils.readLines(file, "UTF-8");

            int wordCount=0;
            Map<String,WordStructure> map= new HashMap<>();
            for (String line : lines) {
                StringTokenizer token=new StringTokenizer(line);
                while(token.hasMoreTokens()){
                    wordCount++;
                    String word=token.nextToken(", ' ?.!:\"\n");
                    String lowerWord = word.toLowerCase();
                    if(map.containsKey(lowerWord)){
                        WordStructure wordStructure=map.get(lowerWord);
                        wordStructure.setTotalCount(wordStructure.getTotalCount() + 1);
                        wordStructure.setWordFrequency(getWordFrequency(wordFrequencyMap, lowerWord));
                        map.put(lowerWord, wordStructure);
                    } else {
                        WordStructure wordStructure = new WordStructure();
                        wordStructure.setWord(lowerWord);
                        wordStructure.setTotalCount(1);
                        wordStructure.setWordFrequency(getWordFrequency(wordFrequencyMap, lowerWord));
                        map.put(lowerWord, wordStructure);
                    }
                }
            }
            System.out.println("总共单词数：" + wordCount);
            sort(map);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static long getWordFrequency(Map<String, Long> wordFrequencyMap, String word) {
        if (wordFrequencyMap.get(word) != null) {
            return wordFrequencyMap.get(word);
        } else if (word.endsWith("s") && wordFrequencyMap.get(word.substring(0, word.length() - 1)) != null) {
            return wordFrequencyMap.get(word.substring(0, word.length() - 1));
        } else if (word.endsWith("es") && wordFrequencyMap.get(word.substring(0, word.length() - 2)) != null) {
            return wordFrequencyMap.get(word.substring(0, word.length() - 2));
        }else if (word.endsWith("ing") && wordFrequencyMap.get(word.substring(0, word.length() - 3)) != null) {
            return wordFrequencyMap.get(word.substring(0, word.length() - 3));
        }
        return 0;
    }

    public static void sort(Map<String,WordStructure> map){
        List<Map.Entry<String, WordStructure>> infoIds = new ArrayList<>(map.entrySet());
        infoIds.sort((o1, o2) -> (o2.getValue().getTotalCount() - o1.getValue().getTotalCount()));
        for (Map.Entry<String, WordStructure> id : infoIds) {
//            System.out.println(id.getKey() + ":" + String.format("%s-%d-%d", id.getValue().getWord(), id.getValue().getTotalCount(), id.getValue().getWordFrequency()));
            System.out.println(String.format("%s\t%d\t%d;", id.getValue().getWord(), id.getValue().getTotalCount(), id.getValue().getWordFrequency()));
//            System.out.println(String.format("%s\t%d", id.getValue().getWord(),  id.getValue().getWordFrequency()));

//            System.out.println(String.format("%s", id.getValue().getWord()));

//            System.out.println(String.format("%d", id.getValue().getWordFrequency()));
        }
    }
}
