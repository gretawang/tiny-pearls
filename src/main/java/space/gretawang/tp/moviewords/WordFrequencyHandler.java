package space.gretawang.tp.moviewords;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordFrequencyHandler {
    private static Map<String, Long> wordFrequencyMap = new HashMap<>();

    private WordFrequencyHandler() {

    }

    public static Map<String, Long> getWordFrequencyMap() {
        if (wordFrequencyMap.size() == 0) {
            File frequencyFile =  new File(WordFrequencyHandler.class.getResource("/").getPath() + "\\wordfrequencies\\word-frequency-60000.txt");
            try {
                List<String> wordList = FileUtils.readLines(frequencyFile, "UTF-8");
                long count = 1;
                for (String word : wordList) {
                    if (wordFrequencyMap.get(word) != null) {
                        continue;
                    }
                    wordFrequencyMap.put(word.toLowerCase(), count);
                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return wordFrequencyMap;
    }
}
