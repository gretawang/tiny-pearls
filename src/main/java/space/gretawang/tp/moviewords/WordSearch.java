package space.gretawang.tp.moviewords;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class WordSearch {
    public static void main(String args[]) {

        File file = new File(WordFrequencyHandler.class.getResource("/").getPath() + "\\subtitles\\Friends-Season01(1994-1995)\\Friends.S01E02.The.One.With.The.Sonogram.At.The.End.DVDRip.AC3.3.2ch.JOG.srt");

        List<String> lines;
        try {
            lines = FileUtils.readLines(file, "UTF-8");

            int count = 1;
            for (String line : lines) {
                if (line.contains("vinc") || line.contains("vict") ) {
                    System.out.printf("vinc: %d - %s\n", count, line);
                }
//                if(line.contains("od") ){
//                    System.out.printf("od: %d - %s\n", count, line);
//                }

                if(line.contains("struct")){
                    System.out.printf("struct: %d - %s\n", count, line);
                }

                if(line.contains("prehend") || line.contains("prehens")){
                    System.out.printf("prehend: %d - %s\n", count, line);
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
