package space.gretawang.tp.moviewords;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ScriptsHandler {
    public static void main(String[] args) {
        try {
            List<String> scriptLines = FileUtils.readLines(new File(WordFrequencyHandler.class.getResource("/").getPath() + "\\subtitles\\[zmk.pw]How.To.Train.Your.Dragon.2010.BluRay.1080p.MultiAudio.DTS-HD.MA.5.1.x264-beAst.eng.srt"), "UTF-8");
            for (String line : scriptLines) {
                if (StringUtils.isNumeric(line) || StringUtils.isEmpty(line) || line.contains(":")) {
                    continue;
                }
                FileUtils.write(new File("C:\\Users\\leandev\\Desktop\\Test.txt"), line+"\n", "UTF-8", true);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
